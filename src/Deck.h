#pragma once
#include "Card.h"
#include <vector>
#include "ofImage.h"
#include "ofVec2f.h"

#define TOTAL_CARDS 12
#define COLS 6
#define ROWS 2

using namespace std;

class Deck {
public:
	ofVec2f pos;
	float offset;
	float cardWidth;
	float cardHeight;

	vector<Card> cards;
	vector<ofImage> images;

	Deck();
	void shuffle();
	void faceDownCards();
	void loadImages();
	void loadCards();
	void placeCards();
	void drawCards();
	Card *getCard(float x, float y);
};