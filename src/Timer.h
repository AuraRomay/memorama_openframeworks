#pragma once

namespace gameutils {
	class Timer {
		public:
			int savedTime;
			int totalTime;

			void setup(int duration);
			void start();
			bool isFinished();
	};

}
