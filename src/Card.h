#pragma once
#include "Sprite.h"
#include "ofRectangle.h"

using namespace gameutils;

class Card {
public:
	ofRectangle rect;
	
	Sprites front;
	Sprites back;
	bool isFaceUp;
	bool isActive;

	Card();
	Card(float x, float y, float w, float h);

	void draw();
	void setImages(ofImage *fImg, ofImage *bImg);
	void setSize(float w, float h);
	void setPosiiton(float x, float y);
	bool isPointInside(float x, float y);
	bool flip();
};