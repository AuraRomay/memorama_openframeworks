#include "PlayState.h"
#include "IntroState.h"
#include "ofGraphics.h"
#include "GameState.h"

void PlayState::setup()
{
	cout << "Entro al Play State" << endl;
}

void PlayState::exit()
{
	cout << "Salio del Play State" << endl;
}

void PlayState::update()
{
}

void PlayState::draw()
{
	ofDrawBitmapString("PlayState", 80, 80);
	ofSetColor(0, 255, 0);
	ofDrawCircle(800, 600, 50);
}

void PlayState::keyPressed(int key)
{
	GameStateManager *stateMgr = GameStateManager::getInstance();
	if (key == 'b')
		stateMgr->changeState(new IntroState());
}

void PlayState::keyReleased(int key)
{
}

void PlayState::mouseMoved(int x, int y)
{
}

void PlayState::mouseDragged(int x, int y, int button)
{
}

void PlayState::mousePressed(int x, int y, int button)
{
}

void PlayState::mouseReleased(int x, int y, int button)
{
}
