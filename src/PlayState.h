#pragma once
#include "GameState.h"
#include "IntroState.h"
#include "GameStateManager.h"
#include <iostream>

using namespace std;

class PlayState : public GameState {
	void setup();
	void exit();
	void update();
	void draw();
	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
};
