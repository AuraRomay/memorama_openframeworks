#include "IntroState.h"
#include "ofGraphics.h"
#include "GameState.h"

void IntroState::setup()
{
	cout << "Entro al intro State" << endl;
}

void IntroState::exit()
{
	cout << "Salio del intro State" << endl;
}

void IntroState::update()
{
}

void IntroState::draw()
{
	ofDrawBitmapString("IntroState", 30, 30);
	ofSetColor(255, 0, 0);
	ofDrawCircle(800, 600, 50);
}

void IntroState::keyPressed(int key)
{
	GameStateManager *stateMgr = GameStateManager::getInstance();
	if(key == 'a')
	stateMgr->changeState(new PlayState());

}

void IntroState::keyReleased(int key)
{
}

void IntroState::mouseMoved(int x, int y)
{
}

void IntroState::mouseDragged(int x, int y, int button)
{
}

void IntroState::mousePressed(int x, int y, int button)
{
}

void IntroState::mouseReleased(int x, int y, int button)
{
}

