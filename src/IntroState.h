#pragma once
#include <iostream>
#include "GameState.h"
#include "GameStateManager.h"
#include "PlayState.h"

using namespace std;

class IntroState : public GameState {
	 void setup();
	 void exit();
	 void update();
	 void draw();
	 void keyPressed(int key);
	 void keyReleased(int key);
	 void mouseMoved(int x, int y);
	 void mouseDragged(int x, int y, int button);
	 void mousePressed(int x, int y, int button);
	 void mouseReleased(int x, int y, int button);
};