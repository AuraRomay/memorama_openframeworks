#include "Card.h"

Card::Card()
{
	rect.set(0, 0, 0, 0);
	front.init(0, 0, 0, 0);
	back.init(0, 0, 0, 0);
	isActive = true;
}

Card::Card(float x, float y, float w, float h)
{
	rect.set(x, y, w, h);
	front.init(x, y, w, h);
	back.init(x, y, w, h);
	isFaceUp = false;
	isActive = true;
}

void Card::draw()
{
	if (isActive) {
		if (isFaceUp) {
			front.draw();
		}
		else {
			back.draw();
		}
	}
}

void Card::setImages(ofImage *fImg, ofImage *bImg)
{
	front.setImage(fImg);
	back.setImage(bImg);
}

void Card::setPosiiton(float x, float y)
{
	rect.setPosition(x, y);
	front.pos.set(x, y);
	back.pos.set(x, y);
}

void Card::setSize(float w, float h)
{
	rect.setSize(w, h);
	front.size.set(w, h);
	back.size.set(w, h);
}

bool Card::isPointInside(float x, float y)
{
	return rect.inside(x, y);
}

bool Card::flip()
{
	isFaceUp = !isFaceUp;
	return true;
}
