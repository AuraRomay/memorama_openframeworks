#include "Deck.h"

Deck::Deck()
{
	cardWidth = 250;
	cardHeight = 400;
	offset = 50;
	pos.set(30, 30);
}

void Deck::shuffle()
{
	std::srand(std::time(0));
	std::random_shuffle(cards.begin(), cards.end());
}

void Deck::faceDownCards()
{
	for (Card &c : cards) {
		c.isFaceUp = false;		//Si pongo & no hago una copia es solo una referencia
	}
}

void Deck::loadImages()
{
	for (int i = 0; i < TOTAL_CARDS / 2; i++) {
		ofImage img;
		if (img.load(ofToString(i) + ".png")) {
			ofLog(OF_LOG_NOTICE, "Se cargo imagen:" + ofToString(i) + ".png");
			images.push_back(img);
		}
	}
	ofImage img;
	img.load("Moon.png");
	images.push_back(img);
}

void Deck::loadCards()
{
	loadImages();
	for (int i = 0; i < TOTAL_CARDS; i++) {
		Card card;
		int imageIndex = i / 2;
		card.setImages(&images[imageIndex], &images.back());
		card.setSize(cardWidth, cardHeight);
		cards.push_back(card);
	}
}

void Deck::placeCards()
{
	int cardIndex = 0;
	for (int i = 0; i < ROWS; i++) {
		for (int j = 0; j < COLS; j++) {
			Card *card = &cards[cardIndex];
			int posx = pos.x + (cardWidth + offset)*j;
			int posy = pos.y + (cardHeight + offset)*i;
			card->setPosiiton(posx, posy);
			cardIndex++;
		}
	}
}

void Deck::drawCards()
{
	//Recorre el vector u el c es una variable temporal para las cartas
	for (Card &c : cards) {
		c.draw();
	}
}

Card * Deck::getCard(float x, float y)
{
	Card *card = NULL;
	for (Card &c : cards) {
		if (c.isPointInside(x, y)) {
			return &c;
		}
	}
	return card;
}
