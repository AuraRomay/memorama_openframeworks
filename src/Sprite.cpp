#include "Sprite.h"

gameutils::Sprites::Sprites()
{
	pos.set(0, 0);
	size.set(0, 0);
	img = NULL;
}

gameutils::Sprites::Sprites(float x, float y, float w, float h, ofImage * img)
{
	pos.set(x, y);
	size.set(w, h);
	this->img = img;
}

void gameutils::Sprites::init(float x, float y, float w, float h)
{
	pos.set(x, y);
	pos.set(w, h);
}

void gameutils::Sprites::draw()
{
	if (img != NULL) {			//Para que no crashe se llama programacion protectiva
		img->draw(pos, size.x, size.y);
	}

}

void gameutils::Sprites::setImage(ofImage *img)
{
	this->img = img;
}

