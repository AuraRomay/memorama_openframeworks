#pragma once
#include <vector>

using namespace std;
#include "GameState.h"

//Patr�n de dise�o Singleton: Una clase que solo permite tener un objeto activo en todo el programa.

class GameStateManager {
private:
	GameStateManager();
	static GameStateManager *_gameStateMgr; //Variable estatica que apunta al unico objeto activo de esta clase. A las variables privadas se les pone _
	vector<GameState*> states;

public:
	//Funcion que puedo acceder sin necesidad de crear una instancia de la clase
	static GameStateManager *getInstance();
	void changeState(GameState *state);
	void draw();
	void update();
	void keyPressed(int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y);
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);

};
