#include "ofApp.h"
#include "Card.h"

Card * ofApp::selectCard(int x, int y)
{
	Card *card = deck.getCard(x, y);
	if (card != NULL) {
		if (card->isActive && !card->isFaceUp) {
			card->flip();
			return card;
		}
	}
	return NULL;
}

void ofApp::startGame()
{
	deck.placeCards();
	timer.setup(600);
}

void ofApp::compareCards()
{
	if (cardOne->front.img == cardTwo->front.img) {
		totalPairs --;
		//matchSnd.play();
		ofSleepMillis(600);
		cardOne->isActive = false;
		cardTwo->isActive = false;
		cardOne = NULL;
		cardTwo = NULL;
	}
	else {
		//errorSnd.play();
		ofSleepMillis(600);
		cardOne->isFaceUp = false;
		cardTwo->isFaceUp = false;
		cardOne = NULL;
		cardTwo = NULL;
		turnsLeft--;
		if (isGameOver()) {
			cout << "PERDISTE" << endl;
			start = false;
		}
		if (resetGame()) {
			deck.shuffle();
			deck.faceDownCards();
			deck.placeCards();
		}
	}
}

bool ofApp::isGameOver()
{
	if (turnsLeft == 0) {
		return true;
	}
	return false;
}

bool ofApp::resetGame()
{
	if (turnsLeft == 15 || turnsLeft == 8) {
		return true;
	}
	return false;
}

bool ofApp::isWin()
{
	if (totalPairs == 0) {
		return true;
	}
	return false;
}

//--------------------------------------------------------------
void ofApp::setup(){
	/*if (img.load("Sailor.png")) {
		cout << "Se carga la imagen" << endl;
	}
	if (img2.load("Mars.png")) {
		cout << "Se carga la imagen2" << endl;
	}*/

	//sprite = new Sprites(10, 10, 100, 200, &img);
	/*card.setPosiiton(50, 50);
	card.setSize(200, 300);
	card.setImages(&img2, &img);*/

	//cardOne = NULL;
	//cardTwo = NULL;

	//deck.loadCards();
	//deck.shuffle();
	//deck.faceDownCards();
	//deck.placeCards();
	//timer.setup(2000);

	//turnsLeft = 2;//deck.cards.size() + 3;
	//totalPairs = deck.cards.size() / 2;
	myFont.load("panda.ttf", 80);
	back.load("back.jpg");
	cardOne = NULL;
	cardTwo = NULL;

	deck.loadCards();
	deck.shuffle();
	deck.faceDownCards();
	turnsLeft = 20;//deck.cards.size() + 3;
	totalPairs = deck.cards.size() / 2;

	if (start == true) {
		startGame();
	}
}

//--------------------------------------------------------------
void ofApp::update(){
	if (start == true) {
		startGame();
	}
	//Valida si el turno acabo
	if (timer.isFinished()) {
		if (cardOne != NULL && cardTwo != NULL) {
			compareCards();
		}
	}

}

//--------------------------------------------------------------
void ofApp::draw(){
	//sprite->draw();
	//card.draw();
	if (!start && !isGameOver()) {
		ofSetColor(255, 255, 255);
		back.draw(0, 0, ofGetWidth(), ofGetHeight());
		ofSetColor(0, 0, 0);
		myFont.drawString("Bienvenido", ofGetWidth()/2 - 220, ofGetHeight()/2);
		myFont.drawString("Oprime 'ENTER' para empezar", ofGetWidth() / 2 - 520, ofGetHeight() / 2 + 150);
		/*ofset*/
	}

	if (start==true) {
		ofSetColor(255, 255, 255);
		deck.drawCards();
		ofSetColor(0, 0, 0);
		ofDrawBitmapString("Turnos: " + ofToString(turnsLeft), ofGetWidth() / 2, ofGetHeight() - 20);
		ofDrawBitmapString("Pares: " + ofToString(totalPairs), ofGetWidth() / 2, ofGetHeight() - 50);
		ofDrawBitmapString("INSTRUCCIONES: Es como cualquier memorama pero a los 15 u 8 turnos las cartas vuelven a cambiar de posicion", 520, ofGetHeight() - 80);
	}

	if (isGameOver()) {
		ofSetColor(255, 255, 255);
		back.draw(0, 0, ofGetWidth(), ofGetHeight());
		ofSetColor(0, 0, 0);
		myFont.drawString("Perdiste, mejor suerte la proxima", ofGetWidth() / 2 - 620, ofGetHeight() / 2);
	};

	if (isWin()) {
		ofSetColor(255, 255, 255);
		back.draw(0, 0, ofGetWidth(), ofGetHeight());
		ofSetColor(0, 0, 0);
		myFont.drawString("GANASTE!", ofGetWidth() / 2 - 220, ofGetHeight() / 2);
	}

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){
	if (key == VK_RETURN) {
		start = true;
	}
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	if (button == OF_MOUSE_BUTTON_LEFT) {
		if (cardOne == NULL) {
			cardOne = selectCard(x, y);
		}
		else if (cardTwo == NULL) {
			cardTwo = selectCard(x, y);
			timer.start();
		}
	}
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
