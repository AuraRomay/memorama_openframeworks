#pragma once
#include "ofVec2f.h"
#include "ofImage.h"

namespace gameutils {

	class Sprites {

	public:
		ofVec2f pos;	//Se destrullen solos los que no son apuntadores
		ofVec2f size;
		ofImage *img;

		Sprites();
		Sprites(float x, float y, float w, float h, ofImage *img);
		void init(float x, float y, float w, float h);
		void draw();
		void setImage(ofImage *img);
	};
}
