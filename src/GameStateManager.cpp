#include "GameStateManager.h"
#include "GameState.h"

//Inicilizando  una variable est�tica
GameStateManager * GameStateManager::_gameStateMgr = new GameStateManager();//mE REGRESA UN APUNTADOR

GameStateManager::GameStateManager()
{
}

//Funcion est�tica. Regresa un apuntador al �nico objeto creado de esta clase
GameStateManager * GameStateManager::getInstance() {
	return _gameStateMgr;
}

void GameStateManager::changeState(GameState *state) {
	if (!states.empty()) {
		states.back()->exit();
		states.pop_back();
	}
	states.push_back(state);
	states.back()->setup();

}

void GameStateManager::draw()
{
	if (!states.empty()) {
		states.back()->draw();
	}
}

void GameStateManager::update() {
	if (!states.empty()) {
		states.back()->update();

	}
}

void GameStateManager::keyPressed(int key)
{
	if (!states.empty()) {
		states.back()->keyPressed(key);
	}
}

void GameStateManager::keyReleased(int key)
{
	if (!states.empty()) {
		states.back()->keyReleased(key);
	}
}

void GameStateManager::mouseMoved(int x, int y)
{
	if (!states.empty()) {
		states.back()->mouseMoved(x, y);
	}
}

void GameStateManager::mouseDragged(int x, int y, int button)
{
	if (!states.empty()) {
		states.back()->mouseDragged(x, y, button);
	}
}

void GameStateManager::mousePressed(int x, int y, int button)
{
	if (!states.empty()) {
		states.back()->mousePressed(x, y, button);
	}
}

void GameStateManager::mouseReleased(int x, int y, int button)
{
	if (!states.empty()) {
		states.back()->mouseReleased(x, y, button);
	}
}


