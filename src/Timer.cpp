#include "Timer.h"
#include "ofUtils.h"

void gameutils::Timer::setup(int duration)
{
	totalTime = duration;
}

void gameutils::Timer::start()
{
	savedTime = ofGetElapsedTimeMillis();
}

bool gameutils::Timer::isFinished()
{
	int passedTime = ofGetElapsedTimeMillis() - savedTime;
	if (passedTime > totalTime) {
		return true;
	}
	else {
		return false;
	}
}
