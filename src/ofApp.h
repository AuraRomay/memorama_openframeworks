#pragma once

#include "ofMain.h"
#include "Sprite.h"
#include "Deck.h"
#include "Timer.h"


using namespace gameutils;

class Card;

class ofApp : public ofBaseApp{

	public:
	/*	Sprites *sprite;
		ofImage img;
		ofImage img2;

		Card card;*/

		Timer timer;

		Deck deck;
		int turnsLeft;
		int totalPairs;
		int scorePairs;
		bool start = false;

		Card *cardOne;
		Card *cardTwo;

		Card* selectCard(int x, int y);
		void startGame();
		void compareCards();

		bool isGameOver();
		bool resetGame();
		bool isWin();

		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);

		ofTrueTypeFont myFont;
		ofImage back;
		
};
